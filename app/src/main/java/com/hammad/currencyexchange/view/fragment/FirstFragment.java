package com.hammad.currencyexchange.view.fragment;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.hammad.currencyexchange.databinding.FragmentFirstBinding;
import com.hammad.currencyexchange.db.CurrencyCode;
import com.hammad.currencyexchange.viewmodel.ExchangeVM;

public class FirstFragment extends Fragment {

    private FragmentFirstBinding binding;
    private ExchangeVM viewModel;
    private String to;
    private String from;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = FragmentFirstBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(this).get(ExchangeVM.class);

        ArrayAdapter adapter = new ArrayAdapter<>(requireContext(),
                android.R.layout.simple_spinner_item,
                CurrencyCode.getCodes());

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.fromSpinner.setAdapter(adapter);
        binding.toSpinner.setAdapter(adapter);
        binding.fromSpinner.setSelection(0);//set USD as Default
        binding.toSpinner.setSelection(1);//set INR as Default

        binding.fromSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                from = CurrencyCode.getCodes().get(position);
                getExchangeRates();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.toSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                to = CurrencyCode.getCodes().get(position);
                getExchangeRates();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.etFrom.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
               if (s != null || !s.toString().isEmpty())
                viewModel.getToValue(s.toString()).observe(
                        getViewLifecycleOwner(),
                        aDouble -> binding.tvToCurrency.setText(String.valueOf(aDouble)));
            }

            @Override
            public void afterTextChanged(Editable s) { }
        });
    }

    private void getExchangeRates() {
        viewModel.getRate(from, to).observe(getViewLifecycleOwner(), exchangeModel -> {
            binding.tvToName.setText(exchangeModel.getToCountry());
            binding.tvFromName.setText(exchangeModel.getFromCountry());
            binding.tvExchangeRate.setText("Exchange Rate: " + exchangeModel.getExchangeRate());
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}