package com.hammad.currencyexchange.db;

import java.util.ArrayList;
import java.util.List;

public class CurrencyCode {

    public static List<String> getCodes() {
        List<String> mList = new ArrayList<>();
        mList.add("USD");
        mList.add("INR");
        mList.add("EUR");
        mList.add("AUD");
        mList.add("CAD");
        mList.add("SAR");
        mList.add("KWD");
        mList.add("MXN");
        mList.add("ZWD");
        mList.add("AFN");
        mList.add("PKR");

        return mList;
    }
}
