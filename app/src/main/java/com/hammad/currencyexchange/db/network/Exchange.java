package com.hammad.currencyexchange.db.network;

import com.hammad.currencyexchange.model.ExchangeModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Exchange {

    @GET("currency/formCurrency/{fc}/toCurrency/{tc}")
    Call<ExchangeModel> getExchanges(@Path("fc") String fromCountry,
                                     @Path("tc") String toCountry);
}
