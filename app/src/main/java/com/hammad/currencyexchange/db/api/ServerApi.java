package com.hammad.currencyexchange.db.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Query;

public class ServerApi {
    public static final String BASE_URL = "https://still-springs-64115.herokuapp.com/";

    public static Retrofit invoke() {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build();
    }
}
