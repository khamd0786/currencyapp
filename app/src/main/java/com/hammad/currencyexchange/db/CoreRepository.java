package com.hammad.currencyexchange.db;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.hammad.currencyexchange.db.api.ServerApi;
import com.hammad.currencyexchange.db.network.Exchange;
import com.hammad.currencyexchange.model.ExchangeModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CoreRepository {
    private static final String TAG = "CoreRepository";
    private Double exchangeRate = 0.00;

    public MutableLiveData<ExchangeModel> getRate(String from, String to) {
        MutableLiveData<ExchangeModel> result = new MutableLiveData<>();
        Exchange exchange = ServerApi.invoke().create(Exchange.class);
        exchange.getExchanges(from, to).enqueue(new Callback<ExchangeModel>() {
            @Override
            public void onResponse(Call<ExchangeModel> call, Response<ExchangeModel> response) {
                ExchangeModel body = response.body();
                if (body != null)
                    if (body.getExchangeRate() != null)
                        exchangeRate = body.getExchangeRate();
                result.postValue(body);
            }

            @Override
            public void onFailure(Call<ExchangeModel> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });
        return result;
    }

    public MutableLiveData<Double> getToValue(String fromCurrency) {
        MutableLiveData<Double> toValue = new MutableLiveData<>(0.00);
        if (fromCurrency != null && !fromCurrency.isEmpty())
            toValue.postValue(getProduct(Double.parseDouble(fromCurrency)));
        return toValue;
    }

    private Double getProduct(Double fromCurrency) {
        if (fromCurrency != null)
            return exchangeRate * fromCurrency;
        return 0.00;
    }

}
