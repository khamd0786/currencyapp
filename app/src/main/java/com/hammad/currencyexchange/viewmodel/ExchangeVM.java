package com.hammad.currencyexchange.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.hammad.currencyexchange.db.CoreRepository;
import com.hammad.currencyexchange.model.ExchangeModel;

public class ExchangeVM extends ViewModel {
    private CoreRepository coreRepository = new CoreRepository();
    private static final String TAG = "ExchangeVM";

    public LiveData<ExchangeModel> getRate(String from, String to) {
        return coreRepository.getRate(from, to);
    }

    public LiveData<Double> getToValue(String fromCurrency) {
        return coreRepository.getToValue(fromCurrency);
    }
}
